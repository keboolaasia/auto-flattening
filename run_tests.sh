#!/bin/sh

echo "Running test suite"

#it's not really a test, but something that makes sure that this thing runs to a completition with sample inputs

docker run --rm \
       -v `pwd`/tests/data/in/tables:/data/in/tables \
       -v `pwd`/config.json:/data/config.json \
       -v `pwd`/tests/data/out/tables:/data/out/tables \
       -e KBC_DATADIR='/data/'\
          kbc-app-flatten-json:dev
