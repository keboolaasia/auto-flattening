<?php

require "vendor/autoload.php";
require_once "tools.php";
use Keboola\Json\Parser;

// Define paths to the files (defaults for Keboola Docker Extensions)
date_default_timezone_set('UTC');
define('IN_PATH', '/data/in/tables/');
define('OUT_PATH', '/data/out/tables/');
define('ROWS_TO_PROCESS', 20000);

// Load configurations from the KBC
$configFile = getenv('KBC_DATADIR') . DIRECTORY_SEPARATOR . 'config.json';
$config = json_decode(file_get_contents($configFile), true);

function checkColumnType($header, $json)
{
	// Fallback to defaults for header
	foreach ($header as $name => $type)
	{
		$header[$name] = 'scalar';
	}

	foreach ($json as $row)
	{
		foreach ($row as $key => $value)
		{
			if ($header[$key] == 'scalar' && (is_object($value) || is_array($value)))
			{
				$header[$key] = 'object';
			}
		}
	}

	foreach ($json as $index => $row)
	{
		foreach ($header as $key => $type)
		{
			if ($type == 'object' && isset($row->$key) && $row->$key == '')
			{
				$json[$index]->$key = json_decode('{}');
			}
		}
	}

	return $json;
}

function process($json, $file, $entityName)
{
	global $config, $tableNames, $db;

	echo "Parsing: ".$entityName."\n";

	if(empty($json))
	{
		echo "WARNING: Skipping ".$entityName." - it is empty.\n";
		return;
	}

	// Create parser and parse the json
	$parser = Parser::create(new \Monolog\Logger('json-parser'));
	$parser->process($json, $entityName);
	$result = $parser->getCsvFiles();

	echo "Copying data (".$entityName.") to final destination.\n";

	// Copy resulting files to the KBC destination
	foreach ($result as $f)
	{
		echo "Inserting table ".$f->getFileName()." to SQLite.\n";
		
		$tableName = substr($f->getFileName(), strpos($f->getFileName(), '-')+1);

		if (!in_array($tableName, $tableNames))
		{
			$tableNames[] = $tableName;
		}

		addNewFile($db, $tableName, $f->getPathName());
	}
}

// function convertFromPythonDump($text)
// {
// 	$text = str_replace(': None,', ': null,', $text);
// 	$text = str_replace("\\'", '{{escaped apostrophe}}', $text);
// 	$text = str_replace('"', 'replace', subject);
// }

function processRow($header, $row)
{
	$obj = [];

	foreach ($header as $k => $i)
	{

		$isJson = json_decode($row[$i]);
		
		// var_dump($row[$i]);
		// echo "\n";
		// var_dump($isJson);
		// echo "\n";
		// echo "\n";

		if ($isJson !== NULL)
		{
			$obj[$k] = $isJson;
		}
		else
		{
			$obj[$k] = $row[$i];
		}
	}

	return (object) $obj;
}

// Main flow
foreach (glob(IN_PATH."*.csv") as $file)
{
	echo "Loading: ".$file."\n";

	// SQLite for consolidating the files
	$db = createDb();
	$tableNames = array();

	// Create name for the entity based on file name
	$entityName = str_replace(IN_PATH, '', str_replace('.csv', '', $file));

	$json = [];
	$counter = 0;
	$header = [];

	$fileDescriptor = fopen($file, 'r');
	
	// Load rows from source table
	while (($data = fgetcsv($fileDescriptor, 0, ",", "\"", "\"")) !== FALSE)
	{
		// Ignore header
		$counter += 1;
		if ($counter == 1 && empty($header))
		{
			// Turning usual array of values (row) to the dictionary of column names and its indices
			$header = array_flip($data);
			continue;
		}

		// Currently we take only first column, presuming it has JSON packet in it
		// $text .= $data[0]."\n";
		$json[] = processRow($header, $data);

		// After reaching number of rows, process data to prevent memory limitation
		if ($counter == ROWS_TO_PROCESS)
		{
			$json = checkColumnType($header, $json);
			process($json, $file, $entityName);
			$json = [];
			$counter = 0;
		}
	}

	fclose($fileDescriptor);

	// If we have some rest, process it as well
	if ($counter > 0)
	{
		$json = checkColumnType($header, $json);
		process($json, $file, $entityName);
	}

	// Exporting SQLite to CSV
	foreach($tableNames as $tableName)
	{
        if (isset($config['parameters']['output_bucket']))
        {
            $outpath = OUT_PATH.$config['parameters']['output_bucket'].".".$tableName.".csv";
        } else {
            $outpath = OUT_PATH.$tableName.".csv";
        }
        echo "exporting ".$tableName." to ".$outpath."\n";
		exportTablesAsCSV($db, $tableName, $outpath);
	}

	echo "Done for: ".$entityName."\n";
}


