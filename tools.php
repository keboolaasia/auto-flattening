<?php

// SQLite Data consolidation
function createDb()
{
	// Create new database in memory
	$memory_db = new PDO('sqlite::memory:');
	// Set errormode to exceptions
	$memory_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	return $memory_db;
}

function addNewFile($db, $tableName, $filePath)
{
	$fd = fopen($filePath, 'r');
	$header = fgetcsv($fd, 0, ",", "\"", "\"");

	createOrAlterTable($db, $tableName, $header);

	$db->query("BEGIN TRANSACTION;");

	while (($values = fgetcsv($fd, 0, ",", "\"", "\"")) !== FALSE)
	{
		insertRow($db, $tableName, $header, $values);
	}

	$db->query("COMMIT;");
}

function insertRow($db, $tableName, $header, $values)
{
	$sql = "INSERT INTO \"".$tableName."\" (";
	$sql .= '"'.implode('", "', $header).'"';
	$sql .= ") VALUES (";
	$sql .= ":".implode(", :", $header);
	$sql .= ")";

	$stmt = $db->prepare($sql);
	
	for($i = 0; $i < count($header); $i++)
	{
		$stmt->bindParam(':'.$header[$i], $values[$i]);
	}

	$stmt->execute();
}

function createOrAlterTable($db, $tableName, $columns)
{
	$sql = "CREATE TABLE IF NOT EXISTS \"".$tableName."\" (";
	$sql .= '"'.implode('" TEXT, "', $columns).'" TEXT';
	$sql .= ")";
	$db->exec($sql);

	$addNewColumns = array();
	$existingColumns = array();

	$result = $db->query('PRAGMA table_info("'.$tableName.'")');
	foreach($result as $row) 
	{
		$existingColumns[] = $row['name'];
	}

	foreach($columns as $column)
	{
		if (!in_array($column, $existingColumns))
		{
			$db->exec('ALTER TABLE "'.$tableName.'" ADD COLUMN "'.$column.'" TEXT');
		}
	}
}

function exportTablesAsCSV($db, $tableName, $filePath)
{
	$fd = fopen($filePath, 'w');

	$result = $db->query('select * from "'.$tableName.'"');

	$writeHeader = true;

	while ($row = $result->fetch(PDO::FETCH_ASSOC)) {
		if ($writeHeader == true)
		{
			fputcsv($fd, array_keys($row));
			$writeHeader = false;
		}

	    fputcsv($fd, $row);
	}

	fclose($fd);
}